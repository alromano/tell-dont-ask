package it.gabrieletondi.telldontaskkata.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Order {
  private BigDecimal total;
  private String currency;
  private List<OrderItem> items;
  private BigDecimal tax;
  private OrderStatus status;
  private int id;

  public Order() {
    this(OrderStatus.CREATED,"EUR",BigDecimal.ZERO,BigDecimal.ZERO);
  }

  public Order(OrderStatus status, String currency, BigDecimal total, BigDecimal tax) {
    this.status = status;
    this.items = new ArrayList<>();
    this.currency = currency;
    this.total = total;
    this.tax = tax;
  }

  public BigDecimal getTotal() {
    return total;
  }


  public String getCurrency() {
    return currency;
  }

  public List<OrderItem> getItems() {
    return items;
  }

  public BigDecimal getTax() {
    return tax;
  }

  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public void add(OrderItem orderItem) {
    items.add(orderItem);
    this.total = total.add(orderItem.getTaxedAmount());
    this.tax = this.tax.add(orderItem.getTax());
  }
}
